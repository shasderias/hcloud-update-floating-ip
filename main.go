package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"net"
	"os"

	"github.com/hetznercloud/hcloud-go/hcloud"
)

const (
	logFilePath = "/var/log/hcloud-update-floating-ip.log"
)

func printUsage() {
	log.Printf("hcloud-update-floating-ip [server name] [floating IP]\n")
}

func main() {
	var logWriter io.Writer

	logFile, err := os.OpenFile(logFilePath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		fmt.Fprintf(os.Stderr, "error opening %s for writing, logging to stderr only\n", logFilePath)
		logWriter = os.Stderr
	} else {
		logWriter = io.MultiWriter(os.Stderr, logFile)
	}

	log.SetOutput(logWriter)

	args := os.Args

	if len(args) != 3 {
		printUsage()
		os.Exit(1)
	}

	targetServerName := args[1]
	targetFloatingIP := net.ParseIP(args[2])
	if targetFloatingIP == nil {
		log.Printf("%s is not a valid IP address\n", targetFloatingIP)
		os.Exit(1)
	}

	token := os.Getenv("HCLOUD_TOKEN")
	if token == "" {
		log.Printf("HCLOUD_TOKEN not set\n")
		os.Exit(1)
	}

	client := hcloud.NewClient(hcloud.WithToken(token))

	servers, err := client.Server.All(context.Background())
	if err != nil {
		log.Printf("error retrieving list of servers\n")
		os.Exit(1)
	}

	floatingIPs, err := client.FloatingIP.All(context.Background())
	if err != nil {
		log.Printf("error retrieving list of floating IPs\n")
		os.Exit(1)
	}

	var server *hcloud.Server
	var floatingIP *hcloud.FloatingIP

	for _, srv := range servers {
		if srv.Name == targetServerName {
			server = srv
		}
	}

	for _, fip := range floatingIPs {
		if fip.IP.Equal(targetFloatingIP) {
			floatingIP = fip
		}
	}

	if server == nil {
		log.Printf("could not find server with name %s\n", targetServerName)
		os.Exit(1)
	}

	if floatingIP == nil {
		log.Printf("could not find floating IP %s\n", targetFloatingIP)
		os.Exit(1)
	}

	if _, _, err := client.FloatingIP.Assign(context.Background(), floatingIP, server); err != nil {
		log.Printf("error assiging floating IP to server: %v\n", err)
		os.Exit(1)
	}

	log.Printf("assigned %s to %s\n", targetFloatingIP, targetServerName)
}
